<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd6f10cdeb47c8cbd235000ee0a226e21
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Chenzeming\\Tools\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Chenzeming\\Tools\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd6f10cdeb47c8cbd235000ee0a226e21::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd6f10cdeb47c8cbd235000ee0a226e21::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitd6f10cdeb47c8cbd235000ee0a226e21::$classMap;

        }, null, ClassLoader::class);
    }
}
