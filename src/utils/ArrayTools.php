<?php


namespace Chenzeming\Tools\utils;


class ArrayTools extends Tools
{
    /**
     * 列表转树形
     *
     * @param array   $list  列表数组
     * @param string  $pk    主键名称
     * @param string  $pid   父键名称
     * @param integer $root  根节点id
     * @param string  $child 子节点名称
     *
     * @return array
     */
    public  function listToTree($list = [], $pk = 'id', $pid = 'pid', $root = 0,  $child = 'children')
    {
        $tree  = [];
        $refer = [];
        foreach ($list as $k => $v) {
            $refer[$v[$pk]] = &$list[$k];
        }
        foreach ($list as $key => $val) {
            $parent_id = 0;
            if (isset($val[$pid])) {
                $parent_id = $val[$pid];
            }
            if ($root == $parent_id) {
                $tree[] = &$list[$key];
            } else {
                if (isset($refer[$parent_id])) {
                    $parent = &$refer[$parent_id];
                    $parent[$child][] = &$list[$key];
                }
            }
        }
        return $tree;
    }

    /**
     * 判断数组中的某个值是否位空
     * @param $arr mixed 数组
     * @param $key mixed 健值名
     * @return bool
     */
    public  function isNotEmpty($arr,$key)
    {
        if (empty($arr) || !is_array($arr) || !isset($arr[$key])){
            return false;
        }
        return !empty($arr[$key]);
    }

    /**
     * 判断数组中莫格只是否为空
     * @param $arr mixed 传入数组
     * @param $key mixed 字段名
     * @return bool
     */
    public  function isEmpty($arr,$key)
    {
        if (empty($arr) || !is_array($arr) || !isset($arr[$key])){
            return true;
        }
        return empty($arr[$key]);
    }

}