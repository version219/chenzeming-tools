<?php


namespace Chenzeming\Tools\utils;


class JsonTools extends Tools
{

    /**
     * 判断字符串是否是json字符串
     * @param string $string 需要判断的字符串
     * @return bool
     */
    public  function isJson($string = '')
    {
        if (empty($string)){
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * jsonEncode
     * @param $data
     * @return false|string
     */
    public  function jsonEncode($data = [])
    {
        return json_encode($data, 256);
    }

    /**
     * jsonDecode
     * @param $data
     * @return mixed
     */
    public static function jsonDecode($data = ''){
        return json_decode($data, true);
    }
}