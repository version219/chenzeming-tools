<?php

namespace Chenzeming\Tools\utils;

class PrintTools extends Tools
{

        /**
        * 打印调试信息
        * @param string $data
        * @param false $status
        */
        public function pr($data = '', $status = false)
        {
            if (empty($data)) {
                var_dump($data);
                die();
            }
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            if ($status !== true) {
                die();
            }
        }

}