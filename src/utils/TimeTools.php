<?php


namespace Chenzeming\Tools\utils;


class TimeTools extends Tools
{
    /**
     * 获取日期 通过时间戳
     * @param mixed $time
     * @return false|string
     */
    public function getDateTime($time = '')
    {
        if (empty($time)){
            $time = time();
        }
        return date('Y-m-d H:i:s',$time);
    }

    /**
     * 获取日期 通过时间戳
     * @param mixed $time
     * @return false|string
     */
    public  function datetime($time = '')
    {
        if (empty($time)){
            $time = time();
        }
        return date('Y-m-d H:i:s',$time);
    }

    /**
     * 检测是否时间戳
     * @param $timestamp
     * @return bool
     */
    public function isTimeStamp($timestamp)

    {
        return ((string) (int) $timestamp === (string)$timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
}